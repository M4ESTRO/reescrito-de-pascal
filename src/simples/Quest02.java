package simples;

import javax.swing.JOptionPane;

/*
 * @author Lu�s Henrique de C. Corr�a
 * @version 1.0
 * */
public class Quest02 {

	/*
	 * Escreva um programa para ler uma temperatura em graus Fahrenheit, calcular e
	 * escrever o valor correspondente em graus Celsius.
	 */
	public static void main(String[] args) {
		try {
			Double fahrenheit = Double.parseDouble(JOptionPane.showInputDialog("Insira o valor em Fahrenheit: "));
			JOptionPane.showMessageDialog(null, String.format("Valor em Celcius: %.2f", (fahrenheit - 32) * 5 / 9),
					"Resultado", JOptionPane.INFORMATION_MESSAGE);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Insira apenas valores num�ricos!", "Erro", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido: \n" + e.toString(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		}
	}

}
