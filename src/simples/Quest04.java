package simples;

import javax.swing.JOptionPane;

/*
 * @author Lu�s Henrique de C. Corr�a
 * @version 1.0
 * */
public class Quest04 {

	/*
	 * Escreva um programa para calcular e imprimir o n�mero de l�mpadas necess�rias
	 * para iluminar um determinado c�modo de uma resid�ncia. Dados de entrada: a
	 * pot�ncia da l�mpada utilizada (em watts), as dimens�es (largura e
	 * comprimento, em metros) do c�modo. Considere que a pot�ncia necess�ria � de
	 * 18 watts por metro quadrado.
	 */

	public static void main(String[] args) {
		try {
			Double watts = Double.parseDouble(JOptionPane.showInputDialog("Insira a pot�ncia da l�mpada: "));
			Double largura = Double.parseDouble(JOptionPane.showInputDialog("Insira a largura: "));
			Double comprimento = Double.parseDouble(JOptionPane.showInputDialog("Insira a comprimento: "));
			Double metroQua = largura * comprimento;
			Double quant = metroQua * (watts / 18);
			
			JOptionPane.showMessageDialog(null, String.format("Quantidade de l�mpadas necess�rias: %.0f", quant),
					"Resultado", JOptionPane.INFORMATION_MESSAGE);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Insira apenas valores num�ricos!", "Erro", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido: \n" + e.toString(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		}
	}
}
