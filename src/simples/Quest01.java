package simples;

import javax.swing.JOptionPane;

/*
 * @author Lu�s Henrique de C. Corr�a
 * @version 1.0
 * */

public class Quest01 {
	//Escreva um programa para ler o raio de um c�rculo, calcular e escrever a sua �rea
	public static void main(String[] args) {
		try {
			Double raio = Double.parseDouble(JOptionPane.showInputDialog("Insira o raio do c�rculo: "));
			JOptionPane.showMessageDialog(null, String.format("�rea do c�rculo: %.2f", Math.PI * Math.pow(raio, 2)));
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Insira apenas valores num�ricos!", "Erro", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido: \n" + e.toString(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		}
		
	}

}
