package simples;

import javax.swing.JOptionPane;

/*
 * @author Lu�s Henrique de C. Corr�a
 * @version 1.0
 * */
public class Quest05 {

	/*
	 * Escreva um programa para ler as dimens�es de uma cozinha retangular
	 * (comprimento, largura e altura), calcular e escrever a quantidade de caixas
	 * de azulejos para se colocar em todas as suas paredes (considere quen�o ser�
	 * descontada a �rea ocupada por portas e janelas). Cada caixa de azulejos
	 * possui 1,5 m2
	 */

	public static void main(String[] args) {
		try {
			Double comprimento = Double.parseDouble(JOptionPane.showInputDialog("Insira a comprimento: "));
			Double largura = Double.parseDouble(JOptionPane.showInputDialog("Insira a largura: "));
			Double altura = Double.parseDouble(JOptionPane.showInputDialog("Insira a altura: "));
			Double area = (comprimento * altura * 2) + (largura * altura * 2);
			Double caixa = (double) Math.round((double) area / 1.5);

			
			JOptionPane.showMessageDialog(null, String.format("Quantidade de caixas necess�rias: %.0f", caixa),
					"Resultado", JOptionPane.INFORMATION_MESSAGE);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Insira apenas valores num�ricos!", "Erro", JOptionPane.ERROR_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido: \n" + e.toString(), "Erro",
					JOptionPane.ERROR_MESSAGE);

		}
	}
}
