package repeticao;

import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Quest01 {

	public static void main(String[] args) {
		Integer a = Integer.parseInt(JOptionPane.showInputDialog("Insira o primeiro valor inteiro: "));
		Integer b = 0;
		Double divisao;
		

		do {
			try {
				b = Integer.parseInt(JOptionPane.showInputDialog("Insira um segundo valor inteiro: "));

			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Insira um valor inteiro!");
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Erro desconhecido!\n" + e.toString());
			}
		} while (b <= 0);
		divisao = (double) a / b;
		
		JOptionPane.showMessageDialog(null, a + "/" + b + " = " + divisao, "RESULTADO",
				JOptionPane.PLAIN_MESSAGE,);

	}

}
